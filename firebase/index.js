import firebase from 'firebase/app'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyBAwvpT72T23Be4y6jz88E4j_YRAfONWUw",
    authDomain: "fuzook-dev-d2fb2.firebaseapp.com",
    databaseURL: "https://fuzook-dev-d2fb2.firebaseio.com",
    storageBucket: "fuzook-dev-d2fb2.appspot.com",
    messagingSenderId: "951574995771",
    projectId: "fuzook-dev-d2fb2"
}

export const db = firebase.initializeApp(config).firestore()